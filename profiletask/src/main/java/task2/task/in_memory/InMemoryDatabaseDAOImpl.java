package task2.task.in_memory;

import task2.task.DAO;

import java.util.LinkedHashSet;
import java.util.Set;

public class InMemoryDatabaseDAOImpl implements DAO {
    private Set<String> saved;

    public InMemoryDatabaseDAOImpl() {
        this.saved = new LinkedHashSet<>();
    }

    @Override
    public void save(String value) {
        saved.add(value);
    }

    @Override
    public String readALL() {
        StringBuilder sb = new StringBuilder();
        for(String s: saved){
            sb.append(s + " ");
        }
        return sb.toString();
    }
}
