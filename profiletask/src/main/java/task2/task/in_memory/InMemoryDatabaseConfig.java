package task2.task.in_memory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import task2.task.DAO;

@Configuration
@Profile("mem")
public class InMemoryDatabaseConfig {
    @Bean
    DAO dao(){
        return new InMemoryDatabaseDAOImpl();
    }
}
