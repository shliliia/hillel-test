package task2.task;

import org.springframework.aop.framework.ProxyFactory;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import task2.task.file_database.FileDatabaseConfig;
import task2.task.in_memory.InMemoryDatabaseConfig;
import task2.task.in_memory.InMemoryDatabaseDAOImpl;

public class Main {
    public static void main(String[] args) {

        InMemoryDatabaseDAOImpl bean = getInMemoryDatabaseImpl();
        bean.save("hello");
    }

    private static InMemoryDatabaseDAOImpl getInMemoryDatabaseImpl() {
        InMemoryDatabaseDAOImpl target = new InMemoryDatabaseDAOImpl();
        ProxyFactory factory = new ProxyFactory();
        factory.setTarget(target);
        factory.addAdvice(new ProfilingInterceptor());
        return (InMemoryDatabaseDAOImpl) factory.getProxy();
    }
}
