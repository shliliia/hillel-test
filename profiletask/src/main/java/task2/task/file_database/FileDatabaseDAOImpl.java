package task2.task.file_database;

import org.springframework.beans.factory.annotation.Value;
import task2.task.DAO;

import javax.annotation.PostConstruct;
import javax.annotation.PreDestroy;
import java.io.*;

public class FileDatabaseDAOImpl implements DAO {
    @Value("C:\\Users\\User\\Documents\\Hillel\\EnterpriseTest\\hillel-test\\profiletask\\src\\main\\java\\com.com.task\\test.txt")
    private String path;
    private File file;

    public void setPath(String path) {
        this.path = path;
    }

    @PostConstruct
    public void init() throws Exception {
        file = new File(path);
        if(!file.createNewFile()){
            throw new RuntimeException("Wrong path, can't create a file");
        }
    }

    @PreDestroy
    public void destroy() {
        if (!file.delete()) {
            System.err.println("ERROR: failed  to delete file.");
        }
    }

    @Override
    public void save(String value) {
        try(BufferedWriter bw = new BufferedWriter(new FileWriter(file, true))) {
            bw.write(value);
        }catch(IOException e){
            e.printStackTrace();
        }
    }

    @Override
    public String readALL() {
        String s = new String();
        StringBuilder sb = new StringBuilder();
        try(BufferedReader br = new BufferedReader(new FileReader(file))) {
            while((s = br.readLine())!=null){
                sb.append(s +" ");
            }
        }catch(IOException e){
            e.printStackTrace();
        }
        return sb.toString();
    }
}
