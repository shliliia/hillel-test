package task2.task.file_database;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import task2.task.DAO;

@Configuration
@Profile("file")
public class FileDatabaseConfig {

    @Bean
    DAO dao(){
        return new FileDatabaseDAOImpl();
    }
}
