package com.task.file_database;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import com.task.DAO;

@Configuration
@Profile("file")
public class FileDatabaseConfig {

    @Bean
    DAO dao(){
        return new FileDatabaseDAOImpl();
    }
}
