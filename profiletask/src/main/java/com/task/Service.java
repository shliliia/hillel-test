package com.task;

import org.springframework.beans.factory.annotation.Autowired;

@org.springframework.stereotype.Service("service")
public class Service {
    private DAO dao;

    @Autowired
    public Service(DAO dao) {
        this.dao = dao;
    }

    public void save(String value){
        dao.save(value);
    }

    public String readALL(){
        return dao.readALL();
    }
}
