package com.task;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.support.GenericApplicationContext;
import com.task.file_database.FileDatabaseConfig;
import com.task.in_memory.InMemoryDatabaseConfig;

public class Main {
    public static void main(String[] args) {

        GenericApplicationContext ctx = new AnnotationConfigApplicationContext(
                FileDatabaseConfig.class,
                InMemoryDatabaseConfig.class,
                Service.class);

        Service service = ctx.getBean("service", Service.class);
        service.save("hello");
        service.save("my");
        service.save("dear");
        System.out.println(service.readALL());
        ctx.close();
    }
}
