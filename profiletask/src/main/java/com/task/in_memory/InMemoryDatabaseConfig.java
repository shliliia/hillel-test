package com.task.in_memory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import com.task.DAO;

@Configuration
@Profile("mem")
public class InMemoryDatabaseConfig {
    @Bean
    DAO dao(){
        return new InMemoryDatabaseDAOImpl();
    }
}
