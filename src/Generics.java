import java.util.Collection;

public class Generics {
    static <T> void putObjectsIntoCollection(T[] array, Collection<T> collection) {
        for (T t : array) {
            collection.add(t);
        }
    }
}
