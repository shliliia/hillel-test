import java.util.concurrent.atomic.AtomicInteger;

public class IncrementSynchronize {
    private int value = 0;

    public int getNextValue() {
        AtomicInteger atomicValue = new AtomicInteger(value);
        return atomicValue.getAndIncrement();
    }

    public int getNextValue1() {
        synchronized (this) {
            return value++;
        }
    }

    public synchronized int getNextValue2() {
        return value++;
    }
}
