import java.io.*;
import java.util.*;

public class DistinctWords {

    static Set<String> getDistinctWords(File file) {
        Set<String> distinctWords = new HashSet<>();
        try (Scanner scanner = new Scanner(file);) {
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                getSetOfWords(line, distinctWords);
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return distinctWords;
    }

    private static void getSetOfWords(String line, Set<String> distinctWords) {
        try (Scanner lineScanner = new Scanner(line)) {
            lineScanner.useDelimiter(" ");
            while (lineScanner.hasNext()) {
                String word = lineScanner.next();
                word = checkWordForSpecialCharacters(word);
                distinctWords.add(word);
            }
        }
    }

    private static String checkWordForSpecialCharacters(String word) {
        word = word.toLowerCase();
        if (!Character.isLetter(word.charAt(0))) {
            word = word.substring(1);
        }
        char[] letters = word.toCharArray();
        int cut = letters.length;
        for (int j = 0; j < letters.length; j++) {
            if (!Character.isLetter(letters[j]) && letters[j] != '-') {
                cut = j;
                break;
            }
        }
        return word.substring(0, cut);
    }
}