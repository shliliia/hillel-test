package xmlconfig;

public class Mathematics implements Subject{

    private static final String NAME = "Mathematics";

    @Override
    public String study() {
        return NAME;
    }
}
