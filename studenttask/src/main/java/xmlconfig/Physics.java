package xmlconfig;

public class Physics implements Subject {
    private static final String NAME = "Physics";

    public String study() {
        return NAME;
    }
}
