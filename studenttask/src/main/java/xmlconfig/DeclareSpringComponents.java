package xmlconfig;

import org.springframework.context.support.GenericXmlApplicationContext;

public class DeclareSpringComponents {
    public static void main(String[] args) {
        GenericXmlApplicationContext ctx = new GenericXmlApplicationContext("classpath:spring/app-context-student.xml");
        Student student = ctx.getBean("student", Student.class);
        student.study();
        ctx.close();
    }
}
