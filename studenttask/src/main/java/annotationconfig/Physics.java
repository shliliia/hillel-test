package annotationconfig;

import org.springframework.stereotype.Component;

@Component
public class Physics implements Subject {
    private static final String NAME = "Physics";

    public String study() {
        return NAME;
    }
}
