package annotationconfig;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class Student {
    @Value("John")
    private String name;
    @Value("19")
    private int age;
    @Autowired
    private Teacher teacher;
    @Autowired
    Map<String, Integer> marks;
    @Autowired
    List<Subject> subjects;

    public Student() {
    }

    public Student(Teacher teacher) {
        this.teacher = teacher;
    }

    public void setTeacher(Teacher teacher) {
        this.teacher = teacher;
    }


    public void setMarks(Map<String, Integer> marks) {
        this.marks = marks;
    }

    public void setSubjects(List<Subject> subjects) {
        this.subjects = subjects;
    }

    public Student(String name, int age, Subject subject) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    void study() {
        System.out.print("My name is " + name + ". I study with teacher " + teacher.getName() + " these subjects: ");
        for (Subject subject : subjects) {
            System.out.print(subject.study() + ", ");
        }
        System.out.println("my marks are: ");
        marks.entrySet().stream().forEach(e -> System.out.println(e.getKey() + ": " + e.getValue()));
    }


    public void setName(String name) {
        this.name = name;
    }

    public void setAge(int age) {
        this.age = age;
    }

}
