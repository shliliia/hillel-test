package annotationconfig;

import org.springframework.stereotype.Component;

@Component
public class Mathematics implements Subject {

    private static final String NAME = "Mathematics";

    @Override
    public String study() {
        return NAME;
    }
}
