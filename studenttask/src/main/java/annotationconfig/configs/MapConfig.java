package annotationconfig.configs;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class MapConfig {

    @Bean("marks")
    public Map<String, Integer> getMarks() {
        Map<String, Integer> marks = new HashMap<>();
        marks.put("Mathematics", 7);
        marks.put("Physics", 12);
        return marks;
    }
}
