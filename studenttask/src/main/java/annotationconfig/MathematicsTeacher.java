package annotationconfig;

import org.springframework.stereotype.Component;

@Component
public class MathematicsTeacher implements Teacher {
    String name = "Blahblah";

    public String getName() {
        return name;
    }

}
