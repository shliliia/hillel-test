package annotationconfig;

public interface Teacher {
    String getName();
}
