package annotationconfig;

import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan
public class DeclareSpringComponents {
    public static void main(String[] args) {
        ApplicationContext ctx = new AnnotationConfigApplicationContext("annotationconfig");
        Student student = ctx.getBean("student", Student.class);
        student.study();
    }
}
