package com.task.web.controller;

import com.task.db.entity.Manufacturer;
import com.task.db.entity.Vehicle;
import com.task.exception.DBException;
import com.task.exception.VehicleValidationException;
import com.task.service.VehicleService;
import com.task.web.validation.VehicleValidator;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class VehicleControllerMockTest extends ControllerBaseTest {
    final Long TEST_ID = 1L;
    final Vehicle TEST_VEHICLE = Vehicle.builder()
            .vehicleMass(1456.0)
            .wheels(4)
            .color("red")
            .engineCapacity(2.5)
            .model("X7")
            .seats(6)
            .vinNumber("test")
            .id(1L)
            .build();


    @MockBean
    private VehicleService vehicleService;

    @MockBean
    private VehicleValidator vehicleValidator;

    @Test
    void whenEmptyVehiclesListShouldRespondOkTest() throws Exception {
        when(vehicleService.findAll()).thenReturn(List.of());

        mockMvc.perform(get("/vehicles").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());
    }

    @Test
    void whenFindAllVehiclesShouldRespondOkAndReturnListTest() throws Exception {
        Vehicle vehicle = Vehicle.builder()
                .vehicleMass(1456.0)
                .wheels(4)
                .color("red")
                .engineCapacity(2.5)
                .model("X7")
                .seats(6)
                .vinNumber("test")
                .id(1L)
                .manufacturer(Manufacturer.builder().companyName("testCompany").build())
                .build();

        when(vehicleService.findAll()).thenReturn(List.of(vehicle));

        mockMvc.perform(get("/vehicles")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].vehicleMass", is(1456.0)))
                .andExpect(jsonPath("$[0].wheels", is(4)))
                .andExpect(jsonPath("$[0].color", is("red")))
                .andExpect(jsonPath("$[0].engineCapacity", is(2.5)))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].model", is("X7")))
                .andExpect(jsonPath("$[0].seats", is(6)))
                .andExpect(jsonPath("$[0].manufacturer", is("testCompany")))
                .andExpect(jsonPath("$[0].vinNumber", is("test")));

    }

    @Test
    void whenCreateManufacturerShouldReturnCreatedTest() throws Exception {
        doAnswer((Answer<Void>) invocation -> null).when(vehicleService).save(TEST_VEHICLE);
        mockMvc.perform(post("/vehicles/manufacturers/{id}", TEST_ID)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(TEST_VEHICLE)))
                .andExpect(status().isCreated());
    }

    @Test
    void whenDeleteManufacturerShouldRespondOkTest() throws Exception {
        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);
        doNothing().when(vehicleService).delete(argumentCaptor.capture());
        mockMvc.perform(delete("/vehicles/{id}", TEST_ID)).andExpect(status().isOk());
        assertEquals(TEST_ID, argumentCaptor.getValue());
    }

    @Test
    void whenUpdateManufacturerShouldRespondOkTest() throws Exception {
        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);
        doAnswer((Answer<Void>) invocation -> null).when(vehicleService)
                .update(argumentCaptor.capture(), any(Vehicle.class));
        mockMvc.perform(put("/vehicles/{id}", TEST_ID).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(TEST_VEHICLE)))
                .andExpect(status().isOk());
        assertEquals(TEST_ID, argumentCaptor.getValue());
    }

    @Test
    void whenDeleteManufacturerShouldRespondBadRequestAndReturnDBExceptionTest() throws Exception {
        doThrow(new DBException("There is no such vehicle")).when(vehicleService).delete(TEST_ID);
        mockMvc.perform(delete("/vehicles/{id}", TEST_ID))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(400)))
                .andExpect(jsonPath("$.error", is("com.task.exception.DBException")))
                .andExpect(jsonPath("$.message", is("There is no such vehicle")))
                .andExpect(jsonPath("$.requestPath", is("/vehicles/1")));
    }

    @Test
    void whenUpdateManufacturerShouldRespondBadRequestAndReturnDBExceptionTest() throws Exception {
        doThrow(new DBException("There is no such vehicle")).when(vehicleService).update(TEST_ID, TEST_VEHICLE);
        mockMvc.perform(put("/vehicles/{id}", TEST_ID).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(TEST_VEHICLE)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(400)))
                .andExpect(jsonPath("$.error", is("com.task.exception.DBException")))
                .andExpect(jsonPath("$.message", is("There is no such vehicle")))
                .andExpect(jsonPath("$.requestPath", is("/vehicles/1")));
    }

    @Test
    void whenCreatVehicleShouldReturnValidationExceptionAndRespondBadRequestTest() throws Exception {
        doThrow(new VehicleValidationException("Vehicle field parameter vinNumber must be provided"))
                .when(vehicleValidator).validate(TEST_VEHICLE);
        mockMvc.perform(post("/vehicles/manufacturers/{id}", TEST_ID)
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(TEST_VEHICLE)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(400)))
                .andExpect(jsonPath("$.error", is("com.task.exception.VehicleValidationException")))
                .andExpect(jsonPath("$.message",
                        is("Vehicle field parameter vinNumber must be provided")))
                .andExpect(jsonPath("$.requestPath", is("/vehicles/manufacturers/1")));
    }

    @Test
    void whenUpdateManufacturerShouldRespondBadRequestAndReturnErrorTest() throws Exception {
        doThrow(new VehicleValidationException("Vehicle field parameter vinNumber must be provided"))
                .when(vehicleService).update(TEST_ID, TEST_VEHICLE);
        mockMvc.perform(put("/vehicles/{id}", TEST_ID).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(TEST_VEHICLE)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(400)))
                .andExpect(jsonPath("$.error", is("com.task.exception.VehicleValidationException")))
                .andExpect(jsonPath("$.message",
                        is("Vehicle field parameter vinNumber must be provided")))
                .andExpect(jsonPath("$.requestPath", is("/vehicles/1")));
    }
}
