package com.task.web.controller;

import com.task.db.entity.Manufacturer;
import com.task.exception.DBException;
import com.task.exception.ManufacturerValidationException;
import com.task.service.ManufacturerService;
import com.task.web.validation.ManufacturerValidator;
import org.junit.jupiter.api.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.stubbing.Answer;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;

import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


public class ManufacturerControllerMockTest extends ControllerBaseTest {

    final Long TEST_ID = 1L;
    final Manufacturer TEST_MANUFACTURER = Manufacturer.builder()
            .address("Detroit")
            .companyName("Ford")
            .carModelName("Ford")
            .foundationYear(1903)
            .id(1L)
            .build();

    @MockBean
    private ManufacturerService manufacturerService;

    @MockBean
    private ManufacturerValidator manufacturerValidator;

    @Test
    void whenEmptyManufacturersListShouldRespondOkTest() throws Exception {
        //when
        when(manufacturerService.findAll()).thenReturn(List.of());

        mockMvc.perform(get("/manufacturers").contentType(MediaType.APPLICATION_JSON))
                .andExpect(jsonPath("$", hasSize(0)))
                .andExpect(status().isOk());
    }

    @Test
    void whenListOfManufacturersListShouldRespondOkAndReturnListTest() throws Exception {
        when(manufacturerService.findAll()).thenReturn(List.of(TEST_MANUFACTURER));

        mockMvc.perform(get("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].address", is("Detroit")))
                .andExpect(jsonPath("$[0].companyName", is("Ford")))
                .andExpect(jsonPath("$[0].carModelName", is("Ford")))
                .andExpect(jsonPath("$[0].foundationYear", is(1903)))
                .andExpect(jsonPath("$[0].id", is(1)));

    }

    @Test
    void whenCreateManufacturerShouldReturnCreatedTest() throws Exception {
        doAnswer((Answer<Void>) invocation -> null).when(manufacturerService).save(TEST_MANUFACTURER);
        mockMvc.perform(post("/manufacturers").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(TEST_MANUFACTURER)))
                .andExpect(status().isCreated());
    }

    @Test
    void whenDeleteManufacturerShouldRespondOkTest() throws Exception {
        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);
        doNothing().when(manufacturerService).delete(argumentCaptor.capture());
        mockMvc.perform(delete("/manufacturers/{id}", TEST_ID)).andExpect(status().isOk());
        assertEquals(TEST_ID, argumentCaptor.getValue());
    }

    @Test
    void whenUpdateManufacturerShouldRespondOkTest() throws Exception {
        ArgumentCaptor<Long> argumentCaptor = ArgumentCaptor.forClass(Long.class);
        doAnswer((Answer<Void>) invocation -> null).when(manufacturerService).update(argumentCaptor.capture(),
                any(Manufacturer.class));
        mockMvc.perform(put("/manufacturers/{id}", TEST_ID).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(TEST_MANUFACTURER)))
                .andExpect(status().isOk());
        assertEquals(TEST_ID, argumentCaptor.getValue());
    }

    @Test
    void whenDeleteManufacturerShouldRespondBadRequestAndReturnDBExceptionTest() throws Exception {
        doThrow(new DBException("There is no such manufacturer")).when(manufacturerService).delete(TEST_ID);
        mockMvc.perform(delete("/manufacturers/{id}", TEST_ID))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(400)))
                .andExpect(jsonPath("$.error", is("com.task.exception.DBException")))
                .andExpect(jsonPath("$.message", is("There is no such manufacturer")))
                .andExpect(jsonPath("$.requestPath", is("/manufacturers/1")));
    }

    @Test
    void whenUpdateManufacturerShouldRespondBadRequestAndReturnDBExceptionTest() throws Exception {
        doThrow(new DBException("There is no such manufacturer")).when(manufacturerService)
                .update(TEST_ID, TEST_MANUFACTURER);
        mockMvc.perform(put("/manufacturers/{id}", TEST_ID).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(TEST_MANUFACTURER)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(400)))
                .andExpect(jsonPath("$.error", is("com.task.exception.DBException")))
                .andExpect(jsonPath("$.message", is("There is no such manufacturer")))
                .andExpect(jsonPath("$.requestPath", is("/manufacturers/1")));
    }

    @Test
    void whenCreatVehicleShouldReturnValidationExceptionAndRespondBadRequestTest() throws Exception {
        doThrow(new ManufacturerValidationException("Manufacturer field parameter companyName must be provided"))
                .when(manufacturerValidator).validate(TEST_MANUFACTURER);
        mockMvc.perform(post("/manufacturers", TEST_ID).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(TEST_MANUFACTURER)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(400)))
                .andExpect(jsonPath("$.error",
                        is("com.task.exception.ManufacturerValidationException")))
                .andExpect(jsonPath("$.message",
                        is("Manufacturer field parameter companyName must be provided")))
                .andExpect(jsonPath("$.requestPath", is("/manufacturers")));
    }

    @Test
    void whenUpdateManufacturerShouldRespondBadRequestAndReturnErrorTest() throws Exception {
        doThrow(new ManufacturerValidationException("Manufacturer field parameter companyName must be provided"))
                .when(manufacturerService).update(TEST_ID, TEST_MANUFACTURER);
        mockMvc.perform(put("/manufacturers/{id}", TEST_ID).contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(TEST_MANUFACTURER)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(400)))
                .andExpect(jsonPath("$.error",
                        is("com.task.exception.ManufacturerValidationException")))
                .andExpect(jsonPath("$.message",
                        is("Manufacturer field parameter companyName must be provided")))
                .andExpect(jsonPath("$.requestPath", is("/manufacturers/1")));
    }

}
