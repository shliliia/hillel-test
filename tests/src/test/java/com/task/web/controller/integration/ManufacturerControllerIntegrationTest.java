package com.task.web.controller.integration;

import com.task.db.dao.ManufacturerDAO;
import com.task.db.entity.Manufacturer;
import com.task.web.controller.ControllerBaseTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.util.List;
import java.util.Objects;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ManufacturerControllerIntegrationTest extends ControllerBaseTest {

    @Autowired
    ManufacturerDAO manufacturerDAO;

    @Test
    void whenEmptyManufacturersListShouldRespondOkTest() throws Exception {

        mockMvc.perform(get("/manufacturers").
                contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO manufacturer (id, address, car_model_name, company_name, foundation_year) " +
                    "VALUES (1, 'Berlin', 'BMW', 'BMW', 1904)")
    })
    @DisplayName("should return 'manufacturer with id 1 and HTTP 200'")
    void whenListOfManufacturersListShouldRespondOkAndReturnListTest() throws Exception {

        mockMvc.perform(get("/manufacturers")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].address", is("Berlin")))
                .andExpect(jsonPath("$[0].companyName", is("BMW")))
                .andExpect(jsonPath("$[0].carModelName", is("BMW")))
                .andExpect(jsonPath("$[0].foundationYear", is(1904)))
                .andExpect(jsonPath("$[0].id", is(1)));

    }

    @Test
    @DisplayName("should return 'HTTP 201'")
    void whenCreateManufacturerShouldReturnCreatedTest() throws Exception {
        Manufacturer expected = Manufacturer.builder()
                .address("Detroit")
                .companyName("Ford")
                .carModelName("Ford")
                .foundationYear(1903)
                .build();

        mockMvc.perform(post("/manufacturers").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(expected)))
                .andExpect(status().isCreated());

        List<Manufacturer> allManufacturers = manufacturerDAO.findAll();
        assertEquals(1, allManufacturers.size());
        Manufacturer actual = allManufacturers.get(0);
        assertEquals(expected.getCompanyName(), actual.getCompanyName());
        assertEquals(expected.getFoundationYear(), actual.getFoundationYear());
        assertEquals(expected.getAddress(), actual.getAddress());
        assertEquals(expected.getCarModelName(), actual.getCarModelName());
        assertTrue(Objects.nonNull(actual.getId()));

    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO manufacturer (id, address, car_model_name, company_name, foundation_year) " +
                    "VALUES (1, 'Berlin', 'BMW', 'BMW', 1904)")
    })
    @DisplayName("should return 'HTTP 200'")
    void whenDeleteManufacturerShouldRespondOkAndDeleteManufacturer() throws Exception {
        mockMvc.perform(delete("/manufacturers/1")).andExpect(status().isOk());
        Assertions.assertTrue(manufacturerDAO.findAll().isEmpty());
    }

    @Test
    @SqlGroup({
            @Sql(statements = "INSERT INTO manufacturer (id, address, car_model_name, company_name, foundation_year) " +
                    "VALUES (1, 'Berlin', 'BMW', 'BMW', 1904)")
    })
    @DisplayName("should return 'HTTP 200'")
    void whenUpdateManufacturerShouldCreateAndRespondOk() throws Exception {
        Manufacturer update = Manufacturer.builder()
                .address("Detroit")
                .companyName("Ford")
                .carModelName("Ford")
                .foundationYear(1903)
                .build();
        mockMvc.perform(put("/manufacturers/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(update)))
                .andExpect(status().isOk());
        Manufacturer updated = manufacturerDAO.findById(1L);
        assertEquals(update.getCompanyName(), updated.getCompanyName());
        assertEquals(update.getCarModelName(), updated.getCarModelName());
        assertEquals(update.getAddress(), updated.getAddress());
        assertEquals(update.getFoundationYear(), updated.getFoundationYear());
    }

    @Test
    void whenCreateManufacturerShouldReturnError() throws Exception {
        Manufacturer manufacturer = Manufacturer.builder()
                .companyName("Ford")
                .carModelName("Ford")
                .foundationYear(1903)
                .build();

        mockMvc.perform(post("/manufacturers").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(manufacturer)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(400)))
                .andExpect(jsonPath("$.error",
                        is("com.task.exception.ManufacturerValidationException")))
                .andExpect(jsonPath("$.message",
                        is("Manufacturer field parameter 'address' must be provided")))
                .andExpect(jsonPath("$.requestPath", is("/manufacturers")));
    }

    @Test
    void whenUpdateManufacturerShouldRespondBadRequestAndReturnError() throws Exception {
        Manufacturer update = Manufacturer.builder()
                .address("Detroit")
                .companyName("Ford")
                .carModelName("Ford")
                .foundationYear(1903)
                .build();

        mockMvc.perform(put("/manufacturers/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(update)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(400)))
                .andExpect(jsonPath("$.error", is("com.task.exception.DBException")))
                .andExpect(jsonPath("$.message", is("There is no such manufacturer")))
                .andExpect(jsonPath("$.requestPath", is("/manufacturers/1")));
    }

    @Test
    void whenDeleteManufacturerShouldRespondBadRequestAndReturnError() throws Exception {
        mockMvc.perform(delete("/manufacturers/1"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(400)))
                .andExpect(jsonPath("$.error", is("com.task.exception.DBException")))
                .andExpect(jsonPath("$.message", is("There is no such manufacturer")))
                .andExpect(jsonPath("$.requestPath", is("/manufacturers/1")));
    }
}
