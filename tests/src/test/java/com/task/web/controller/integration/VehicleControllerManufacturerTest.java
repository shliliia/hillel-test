package com.task.web.controller.integration;

import com.task.db.dao.VehicleDAO;
import com.task.db.entity.Vehicle;
import com.task.web.controller.ControllerBaseTest;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.jdbc.SqlGroup;

import java.util.List;
import java.util.Objects;

import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class VehicleControllerManufacturerTest extends ControllerBaseTest {

    @Autowired
    VehicleDAO vehicleDAO;

    @Test
    void whenEmptyVehiclesListShouldRespondOkTest() throws Exception {

        mockMvc.perform(get("/vehicles").
                contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk());
    }

    @Test
    @SqlGroup({
            @Sql(statements =
                    "INSERT INTO manufacturer (id, address, car_model_name, company_name, foundation_year) " +
                            "VALUES (1, 'Berlin', 'BMW', 'BMW', 1904); " +
                            "INSERT INTO vehicle (id, engine_capacity, seats, wheels, vin, mass, color, model, " +
                            "manufacturer_id) VALUES (1, 2.5, 6, 4,'test', 1456.0, 'red', 'X7', 1)")
    })
    @DisplayName("should return 'vehicle with id 1 and HTTP 200'")
    void whenListOfVehiclesShouldRespondOkAndReturnListTest() throws Exception {

        mockMvc.perform(get("/vehicles")
                .contentType(MediaType.APPLICATION_JSON_UTF8))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(1)))
                .andExpect(jsonPath("$[0].engineCapacity", is(2.5)))
                .andExpect(jsonPath("$[0].seats", is(6)))
                .andExpect(jsonPath("$[0].wheels", is(4)))
                .andExpect(jsonPath("$[0].vinNumber", is("test")))
                .andExpect(jsonPath("$[0].id", is(1)))
                .andExpect(jsonPath("$[0].vehicleMass", is(1456.0)))
                .andExpect(jsonPath("$[0].color", is("red")))
                .andExpect(jsonPath("$[0].model", is("X7")))
                .andExpect(jsonPath("$[0].manufacturer", is("BMW")));

    }

    @Test
    @SqlGroup({
            @Sql(statements =
                    "INSERT INTO manufacturer (id, address, car_model_name, company_name, foundation_year) " +
                            "VALUES (1, 'Berlin', 'BMW', 'BMW', 1904)")
    })
    @DisplayName("should return 'HTTP 201'")
    void whenCreateVehicleShouldReturnCreatedTest() throws Exception {
        Vehicle vehicle = Vehicle.builder()
                .vehicleMass(1456.0)
                .wheels(4)
                .color("red")
                .engineCapacity(2.5)
                .model("X7")
                .seats(6)
                .vinNumber("test")
                .build();

        mockMvc.perform(post("/vehicles/manufacturers/1").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(vehicle)))
                .andExpect(status().isCreated());

        List<Vehicle> allVehicles = vehicleDAO.findAll();
        assertEquals(1, allVehicles.size());
        Vehicle actual = allVehicles.get(0);
        assertEquals(vehicle.getColor(), actual.getColor());
        assertEquals(vehicle.getEngineCapacity(), actual.getEngineCapacity());
        assertEquals(vehicle.getModel(), actual.getModel());
        assertEquals(vehicle.getSeats(), actual.getSeats());
        assertEquals(vehicle.getVehicleMass(), actual.getVehicleMass());
        assertEquals(vehicle.getVinNumber(), actual.getVinNumber());
        assertEquals(vehicle.getWheels(), actual.getWheels());
        assertTrue(Objects.nonNull(actual.getId()));

    }

    @Test
    @SqlGroup({
            @Sql(statements =
                    "INSERT INTO manufacturer (id, address, car_model_name, company_name, foundation_year) " +
                            "VALUES (1, 'Berlin', 'BMW', 'BMW', 1904); " +
                            "INSERT INTO vehicle (id, engine_capacity, seats, wheels, vin, mass, color, model, " +
                            "manufacturer_id) VALUES (1, 2.5, 6, 4,'test', 1456.0, 'red', 'X7', 1)")
    })
    @DisplayName("should return 'HTTP 200'")
    void whenDeleteVehicleShouldRespondOkAndDeleteVehicle() throws Exception {
        mockMvc.perform(delete("/vehicles/1")).andExpect(status().isOk());
        Assertions.assertTrue(vehicleDAO.findAll().isEmpty());
    }

    @Test
    @SqlGroup({
            @Sql(statements =
                    "INSERT INTO manufacturer (id, address, car_model_name, company_name, foundation_year) " +
                            "VALUES (1, 'Berlin', 'BMW', 'BMW', 1904); " +
                            "INSERT INTO vehicle (id, engine_capacity, seats, wheels, vin, mass, color, model, " +
                            "manufacturer_id) VALUES (1, 2.5, 6, 4,'test', 1456.0, 'red', 'X7', 1)")
    })
    @DisplayName("should return 'HTTP 200'")
    void whenUpdateVehicleShouldUpdateAndRespondOk() throws Exception {
        Vehicle vehicle = Vehicle.builder()
                .vehicleMass(1456.0)
                .wheels(4)
                .color("red")
                .engineCapacity(2.5)
                .model("X7")
                .seats(6)
                .vinNumber("1111")
                .id(1L)
                .build();

        mockMvc.perform(put("/vehicles/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(vehicle)))
                .andExpect(status().isOk());

        Vehicle updated = vehicleDAO.findById(1L);
        assertEquals(vehicle.getColor(), updated.getColor());
        assertEquals(vehicle.getEngineCapacity(), updated.getEngineCapacity());
        assertEquals(vehicle.getModel(), updated.getModel());
        assertEquals(vehicle.getSeats(), updated.getSeats());
        assertEquals(vehicle.getVehicleMass(), updated.getVehicleMass());
        assertEquals("1111", updated.getVinNumber());
        assertEquals(vehicle.getWheels(), updated.getWheels());
    }

    @Test
    @SqlGroup({
            @Sql(statements =
                    "INSERT INTO manufacturer (id, address, car_model_name, company_name, foundation_year) " +
                            "VALUES (1, 'Berlin', 'BMW', 'BMW', 1904)")
    })
    void whenCreateVehicleShouldReturnError() throws Exception {
        Vehicle vehicle = Vehicle.builder()
                .vehicleMass(1456.0)
                .wheels(4)
                .color("red")
                .model("X7")
                .seats(6)
                .vinNumber("1111")
                .id(1L)
                .build();

        mockMvc.perform(post("/vehicles/manufacturers/1").contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(vehicle)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(400)))
                .andExpect(jsonPath("$.error", is("com.task.exception.VehicleValidationException")))
                .andExpect(jsonPath("$.message", is("Vehicle field parameter 'engineCapacity' " +
                        "must be provided")))
                .andExpect(jsonPath("$.requestPath", is("/vehicles/manufacturers/1")));
    }

    @Test
    void whenUpdateVehicleShouldRespondBadRequestAndReturnError() throws Exception {
        Vehicle vehicle = Vehicle.builder()
                .vehicleMass(1456.0)
                .wheels(4)
                .color("red")
                .engineCapacity(2.5)
                .model("X7")
                .seats(6)
                .vinNumber("1111")
                .id(1L)
                .build();

        mockMvc.perform(put("/vehicles/1")
                .contentType(MediaType.APPLICATION_JSON_UTF8)
                .content(OBJECT_MAPPER.writeValueAsString(vehicle)))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(400)))
                .andExpect(jsonPath("$.error", is("com.task.exception.DBException")))
                .andExpect(jsonPath("$.message", is("There is no such vehicle")))
                .andExpect(jsonPath("$.requestPath", is("/vehicles/1")));
    }

    @Test
    void whenDeleteVehicleShouldRespondBadRequestAndReturnError() throws Exception {
        mockMvc.perform(delete("/vehicles/1"))
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.statusCode", is(400)))
                .andExpect(jsonPath("$.error", is("com.task.exception.DBException")))
                .andExpect(jsonPath("$.message", is("There is no such vehicle")))
                .andExpect(jsonPath("$.requestPath", is("/vehicles/1")));
    }
}
