package com.task.service;

import com.task.db.entity.Vehicle;

import java.util.List;

public interface VehicleService {

    List<Vehicle> findAll();

    Vehicle save(Vehicle vehicle);

    void createVehicleForManufacturer(Long manufacturerId, Vehicle vehicle);

    void delete(Long id);

    Vehicle update(Long id, Vehicle vehicle);
}
