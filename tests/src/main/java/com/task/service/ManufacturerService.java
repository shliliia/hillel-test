package com.task.service;

import com.task.db.entity.Manufacturer;

import java.util.List;

public interface ManufacturerService {


    Manufacturer findById(Long id);

    Manufacturer save(Manufacturer manufacturer);

    List<Manufacturer> findAll();

    void delete(Long id);

    Manufacturer update(Long id, Manufacturer manufacturer);

}
