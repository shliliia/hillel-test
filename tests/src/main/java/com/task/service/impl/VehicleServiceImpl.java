package com.task.service.impl;

import com.task.db.dao.ManufacturerDAO;
import com.task.db.dao.VehicleDAO;
import com.task.db.entity.Manufacturer;
import com.task.db.entity.Vehicle;
import com.task.exception.DBException;
import com.task.service.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Transactional
@Service
public class VehicleServiceImpl implements VehicleService {

    private VehicleDAO vehicleDAO;
    private ManufacturerDAO manufacturerDAO;

    @Autowired
    public VehicleServiceImpl(VehicleDAO vehicleDAO, ManufacturerDAO manufacturerDAO) {
        this.vehicleDAO = vehicleDAO;
        this.manufacturerDAO = manufacturerDAO;
    }

    @Override
    public List<Vehicle> findAll() {
        return vehicleDAO.findAll();
    }

    @Override
    public Vehicle save(Vehicle vehicle) {
        return vehicleDAO.save(vehicle);
    }

    @Override
    public void createVehicleForManufacturer(Long manufacturerId, Vehicle vehicle) {
        Manufacturer manufacturer = manufacturerDAO.findById(manufacturerId);
        vehicle.setManufacturer(manufacturer);
        manufacturer.getVehicles().add(vehicle);
        manufacturerDAO.save(manufacturer);
    }

    @Override
    public void delete(Long id) {
        vehicleDAO.delete(id);
    }

    @Override
    public Vehicle update(Long id, Vehicle vehicle) {
        try{
        Vehicle v = vehicleDAO.findById(id);
        v.setColor(vehicle.getColor());
        v.setEngineCapacity(vehicle.getEngineCapacity());
        v.setModel(vehicle.getModel());
        v.setSeats(vehicle.getSeats());
        v.setVehicleMass(vehicle.getVehicleMass());
        v.setVinNumber(vehicle.getVinNumber());
        v.setWheels(vehicle.getWheels());
        return vehicleDAO.save(v);
        }catch (NoResultException ex){
            throw new DBException("There is no such vehicle", ex);
        }
    }

}
