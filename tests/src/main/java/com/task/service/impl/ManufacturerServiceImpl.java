package com.task.service.impl;

import com.task.db.dao.ManufacturerDAO;
import com.task.db.dao.VehicleDAO;
import com.task.db.entity.Manufacturer;
import com.task.exception.DBException;
import com.task.service.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.persistence.NoResultException;
import javax.transaction.Transactional;
import java.util.List;

@Transactional
@Service
public class ManufacturerServiceImpl implements ManufacturerService {
    private VehicleDAO vehicleDAO;
    private ManufacturerDAO manufacturerDAO;

    @Autowired
    public ManufacturerServiceImpl(VehicleDAO vehicleDAO, ManufacturerDAO manufacturerDAO) {
        this.vehicleDAO = vehicleDAO;
        this.manufacturerDAO = manufacturerDAO;
    }

    @Override
    public Manufacturer findById(Long id) {
        return manufacturerDAO.findById(id);
    }

    @Override
    public Manufacturer save(Manufacturer manufacturer) {
        return manufacturerDAO.save(manufacturer);
    }

    @Override
    public List<Manufacturer> findAll() {
        return manufacturerDAO.findAll();
    }

    @Override
    public void delete(Long id) {
        manufacturerDAO.delete(id);
    }

    @Override
    public Manufacturer update(Long id, Manufacturer manufacturer) {
        try{
            Manufacturer m = findById(id);
            m.setAddress(manufacturer.getAddress());
            m.setCarModelName(manufacturer.getCarModelName());
            m.setCompanyName(manufacturer.getCompanyName());
            m.setFoundationYear(manufacturer.getFoundationYear());
            return manufacturerDAO.save(m);
        }catch (NoResultException ex){
            throw new DBException("There is no such manufacturer", ex);
        }

    }
}
