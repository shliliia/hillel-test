package com.task.db.dao;

import com.task.db.entity.Vehicle;

import java.util.List;

public interface VehicleDAO {
    List<Vehicle> findAll();
    Vehicle findById(Long id);
    Vehicle save(Vehicle vehicle);
    void delete(Long id);
}
