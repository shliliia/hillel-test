package com.task.db.dao.impl;

import com.task.db.dao.ManufacturerDAO;
import com.task.db.entity.Manufacturer;
import com.task.exception.DBException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.List;


@Repository("manufacturerDAO")
public class ManufacturerDAOImpl implements ManufacturerDAO {

    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Manufacturer> findAll() {
        return em.createQuery("select m from Manufacturer m").getResultList();
    }

    @Override
    public Manufacturer findById(Long id) {
        return (Manufacturer) em.createQuery("select m from Manufacturer m where m.id=:id")
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Manufacturer save(Manufacturer manufacturer) {
        try {
            em.persist(manufacturer);
        } catch (RuntimeException e) {
            throw new DBException("Cannot create manufacturer", e);
        }
        return manufacturer;
    }

    @Override
    public void delete(Long id) {
        try{
            Manufacturer manufacturer = findById(id);
            em.remove(manufacturer);
        }catch (NoResultException ex){
            throw new DBException("There is no such manufacturer", ex);
        }
    }


}
