package com.task.db.dao;

import com.task.db.entity.Manufacturer;

import java.util.List;

public interface ManufacturerDAO {
    List<Manufacturer> findAll();

    Manufacturer findById(Long id);

    Manufacturer save(Manufacturer manufacturer);

    void delete(Long id);

}
