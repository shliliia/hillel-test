package com.task.db.dao.impl;

import com.task.db.dao.VehicleDAO;
import com.task.db.entity.Vehicle;
import com.task.exception.DBException;
import org.springframework.stereotype.Repository;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import java.util.List;

@Repository("vehicleDAO")
public class VehicleDAOImpl implements VehicleDAO {
    @PersistenceContext
    private EntityManager em;

    @Override
    public List<Vehicle> findAll() {
        return em.createQuery("select v from Vehicle v").getResultList();
    }

    @Override
    public Vehicle findById(Long id) {
        return (Vehicle) em.createQuery("select v from Vehicle v where v.id=:id")
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public Vehicle save(Vehicle vehicle) {
        em.persist(vehicle);
        return vehicle;
    }

    @Override
    public void delete(Long id) {
        try {
            Vehicle vehicle = findById(id);
            em.remove(vehicle);
        } catch (NoResultException ex) {
            throw new DBException("There is no such vehicle", ex);
        }
    }
}
