package com.task.db.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.*;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@NoArgsConstructor
@AllArgsConstructor
@Builder
@Getter
@Setter
@EqualsAndHashCode(of = "id")
@Entity
@Table(name = "manufacturer")
public class Manufacturer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Setter(AccessLevel.NONE)
    private long id;

    @Column(name = "company_name", unique = true)
    private String companyName;

    @Column(name = "car_model_name")
    private String carModelName;

    @Column(name = "address")
    private String address;

    @Column(name = "foundation_year")
    private int foundationYear;

    @OneToMany(mappedBy = "manufacturer", cascade = CascadeType.ALL)
    @JsonIgnore
    private Set<Vehicle> vehicles= new HashSet<>();
}
