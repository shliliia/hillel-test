package com.task.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.BAD_REQUEST)
public class DBException extends ApplicationGlobalException {
    public DBException(String message){
        super(message);
    }
    public DBException(String message, Throwable cause){
        super(message, cause);
    }
}
