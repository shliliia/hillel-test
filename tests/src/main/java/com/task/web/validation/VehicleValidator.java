package com.task.web.validation;

import com.task.db.entity.Vehicle;
import com.task.exception.VehicleValidationException;
import org.springframework.stereotype.Component;
import org.springframework.util.StringUtils;

@Component
public class VehicleValidator {
    private static final String EMPTY_PROPERTY_EXCEPTION_MESSAGE = "Vehicle field parameter '%s' must be provided";

    public void validate(Vehicle vehicle) throws VehicleValidationException {
        validateNotEmptyProperty(vehicle.getEngineCapacity(), "engineCapacity");
        validateNotEmptyProperty(vehicle.getColor(), "color");
        validateNotEmptyProperty(vehicle.getVehicleMass(), "vehicleMass");
        validateNotEmptyProperty(vehicle.getModel(), "model");
        validateNotEmptyProperty(vehicle.getSeats(), "seats");
        validateNotEmptyProperty(vehicle.getWheels(), "wheels");
        validateNotEmptyProperty(vehicle.getVinNumber(), "vinNumber");
    }

    private void validateNotEmptyProperty(Object value, String propertyName) {
        if (value == null || StringUtils.isEmpty(value)) {
            throw new VehicleValidationException(String.format(EMPTY_PROPERTY_EXCEPTION_MESSAGE, propertyName));
        }
    }
}
