package com.task.web.controller;

import com.task.db.entity.Manufacturer;
import com.task.service.ManufacturerService;
import com.task.web.validation.ManufacturerValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/manufacturers")
public class ManufacturerController {

    private final ManufacturerService manufacturerService;
    private final ManufacturerValidator manufacturerValidator;

    @Autowired
    public ManufacturerController(ManufacturerService manufacturerService, ManufacturerValidator manufacturerValidator) {
        this.manufacturerService = manufacturerService;
        this.manufacturerValidator = manufacturerValidator;
    }

    @PostMapping(consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity create(@RequestBody Manufacturer manufacturer) {
        manufacturerValidator.validate(manufacturer);
        manufacturerService.save(manufacturer);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping(produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<Manufacturer>> getAll() {
        return new ResponseEntity<>(manufacturerService.findAll(), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id) {
        manufacturerService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);

    }

    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity update(@PathVariable("id") Long id, @RequestBody Manufacturer manufacturer) {
        manufacturerValidator.validate(manufacturer);
        manufacturerService.update(id, manufacturer);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
