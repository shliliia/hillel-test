package com.task.web.controller;

import com.task.db.entity.Vehicle;
import com.task.service.VehicleService;
import com.task.web.dto.VehicleDto;
import com.task.web.validation.VehicleValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/vehicles")
public class VehicleController {
    private final VehicleService vehicleService;
    private final VehicleValidator vehicleValidator;

    @Autowired
    public VehicleController(VehicleService vehicleService, VehicleValidator vehicleValidator){
        this.vehicleService = vehicleService;
        this.vehicleValidator = vehicleValidator;
    }

    @PostMapping("/manufacturers/{id}")
    public ResponseEntity<Void> create(@PathVariable("id") Long id, @RequestBody Vehicle vehicle){
        vehicleValidator.validate(vehicle);
        vehicleService.createVehicleForManufacturer(id,vehicle);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    @GetMapping(produces= MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<List<VehicleDto>> getAll(){
        List<VehicleDto> vehicleDtos = vehicleService.findAll()
                .stream()
                .map(VehicleDto::from)
                .collect(Collectors.toList());
        return new ResponseEntity<>(vehicleDtos, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable("id") Long id){
        vehicleService.delete(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @PutMapping("/{id}")
    public ResponseEntity update(@PathVariable Long id, @RequestBody Vehicle vehicle){
        vehicleValidator.validate(vehicle);
        vehicleService.update(id, vehicle);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
