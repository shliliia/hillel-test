package com.task.web.dto;

import com.task.db.entity.Vehicle;
import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class VehicleDto {
    private Long id;

    private Double engineCapacity;

    private Integer seats;

    private Integer wheels;

    private String vinNumber;

    private Double vehicleMass;

    private String color;

    private String model;

    private String manufacturer;

    public static VehicleDto from(Vehicle vehicle) {
        return VehicleDto.builder()
                .color(vehicle.getColor())
                .engineCapacity(vehicle.getEngineCapacity())
                .manufacturer(vehicle.getManufacturer().getCompanyName())
                .model(vehicle.getModel())
                .seats(vehicle.getSeats())
                .vehicleMass(vehicle.getVehicleMass())
                .wheels(vehicle.getWheels())
                .vinNumber(vehicle.getVinNumber())
                .id(vehicle.getId())
                .build();
    }
}
